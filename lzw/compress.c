#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdio.h>
#include"table.h"
void writeheader(int fdw, int* arr){
	int i, bits = 0;
	unsigned char w = '\0';
	for(i = 0; i < 256; i++){
		if(arr[i] == 0){
			w = w & 0xfe;
			bits++;
		}
		else if(arr[i] == 1){
			w = w | 0x01;
			bits++;
		}
		if(bits == 8){
			write(fdw, &w, sizeof(w));
			w = '\0';
			bits = 0;
		}
		w = w << 1;
	}
}
void compress(int fdr, int fdw, table* t, int* arr){
	int index = -1, bits = 0, x, len, i;
	unsigned char ch, w = '\0';
	writeheader(fdw, arr);
	while(read(fdr, &ch, sizeof(ch))){
		x = search(t, index, ch);
		if(x != -1){
			index = x;
		}
		else{
			if(!isfull(t)){
				addentry(t, index, ch, 0);
			}
			char* str = givebinary(index, t->bitsize);
			len = strlen(str);
			for(i = 0; i < len; i++){
				if(str[i] == '0'){
					w = w & 0xfe;
					bits++;
				}
				else{
					w = w | 0x01;
					bits++;
				}
				if(bits == 8){
					write(fdw, &w, sizeof(w));
					w = '\0';
					bits = 0;
				}
				w = w << 1;
			}
			if(isfull(t)){
				inittable(t, arr, 0);
			}
			index = search(t, -1, ch);
		}
	}
	char* str = givebinary(index, t->bitsize);
	len = strlen(str);
	for(i = 0; i < len; i++){
		if(str[i] == '0'){
			w = w & 0xfe;
			bits++;
		}
		else{
			w = w | 0x01;
			bits++;
		}
		if(bits == 8){
			write(fdw, &w, sizeof(w));
			w = '\0';
			bits = 0;
		}
		w = w << 1;
	}
	if(bits < 8 && bits > 0){
		while(bits < 7){
			w = w & 0xfe;
			w = w << 1;
			bits++;
		}
		write(fdw, &w, sizeof(w));
	}
}
