#include<stdio.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<stdlib.h>
#include"table.h"
void readheader(int fdr, int* arr){
	int i, j, k, p = 0, n;
	unsigned char w = '\0';
	for(i = 0; i < 32; i++){
		n = read(fdr, &w, sizeof(w));
		for(j = 0; j < 8; j++){
			k = w & 0x80;
			if(k == 0){
				arr[p] = 0;
				p++;
			}
			else if(k != 0){
				arr[p] = 1;
				p++;
			}
			w = w << 1;
		}
	}
}
typedef unsigned char uc;
/*caller should free the return value*/
uc* findstring(table* t, int index){
	uc* str = (uc*)malloc(sizeof(uc) * 1094);
	int i = 0;
	while(index != -1){
		str[i++] = t->th[index].B;
		index = t->th[index].prefix;	
	}
	str[i] = '\0';
	return str;
}
void writestring(int fdw, uc* str){
	int len = strlen(str);
	int n = len - 1;
	while(n >= 0){
		write(fdw, &(str[n]), sizeof(str[n]));
		n--;
	}
}
int indexmpt(int fdw, int* old, int index, table* t, int* arr){
	uc* str;
	uc* str1;
	uc B;
	int x, n;
	if(index < t->n){
		str = findstring(t, index);
		writestring(fdw, str);
		n = strlen(str);
		B = str[n - 1];
		if(!isfull(t))
			addentry(t, *old, B, 1);
		free(str);
	}
	else{
		str = findstring(t, *old);
		n = strlen(str);
		B = str[n - 1];
		if(!isfull(t))
			addentry(t, *old, B, 1);
		free(str);
		x = (t->n - 1);
		str1 = findstring(t, x);
		writestring(fdw, str1);
		free(str1);
	}
	*old = index;
}
void decode(int fdr, int fdw, table* t, int* arr){
	uc w, B;
	uc* str;
	int k, bits = 0, i, index, old, m = 0, c = 0;
	read(fdr, &w, sizeof(w));
	for(i = 0; i < 8; i++){
		k = w & 0x80;
		if(k == 0){
			m = m & 0xfffffffe;
			bits++;
		}
		else if(k != 0){
			m = m | 0x00000001;
			bits++;
		}
		if(bits == t->bitsize){
			c++;
			index = m;
			str = findstring(t, index);
			writestring(fdw, str);
			if(c > 1){
				B = str[0];
				if(!isfull(t))
					addentry(t, old, B, 1);
			}
			free(str);
			old = index;
			m = 0;
			bits = 0;
		}
		m = m << 1;
		w = w << 1;
	}
	while(read(fdr, &w, sizeof(w))){
		for(i = 0; i < 8; i++){
			k = w & 0x80;
			if(k == 0){
				m = m & 0xfffffffe;
				bits++;
			}
			else if(k != 0){
				m = m | 0x00000001;
				bits++;
			}
			if(bits == t->bitsize){
				index = m;
				indexmpt(fdw, &old, index, t, arr);
				m = 0;
				bits = 0;
			}
			m = m << 1;
			w = w << 1;
		}	
		if(t->bitsize == 13){
			inittable(t, arr, 1);
			c = 0;	
			read(fdr, &w, sizeof(w));
			for(i = 0; i < 8; i++){
				k = w & 0x80;
				if(k == 0){
					m = m & 0xfffffffe;
					bits++;
				}
				else if(k != 0){
					m = m | 0x00000001;
					bits++;
				}
				if(bits == t->bitsize){
					c++;
					index = m;
					str = findstring(t, index);
					writestring(fdw, str);
					if(c > 1){
						B = str[0];
						if(!isfull(t))
							addentry(t, old, B, 1);
					}
					free(str);
					old = index;
					m = 0;
					bits = 0;
				}
				m = m << 1;
				w = w << 1;
			}
		}
	}
}

