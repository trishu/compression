#include<fcntl.h>
#include<unistd.h>
#include<math.h>
#include<stdlib.h>
#include<stdio.h>
void counttablesize(int fdr, int* arr){
	unsigned char ch;
	while(read(fdr, &ch, sizeof(ch))){
		int i = (int)ch;
		if(arr[i] != 1){
			arr[i] = 1;
		}
	}
}
int FindBitRequire(int num, int type){
	float x;
	int y, z;
	if(type == 0){
		x = log(num + 1) / log(2);
		y = (int)x;
		if(x > y)
			return y+1;
		else
			return y;
	}	
	else if(type == 1){
		x = log(num + 2) / log(2);
		y = (int)x;
		if(x > y)
			return y + 1;
		else
			return y;
	}
	return -1;
}
int FindMaxValue(int n, int type){
	if(type == 0)
		return (int)(pow(2, n) - 1);
	else if(type == 1)
		return (int)(pow(2, n) - 2);
	return -1;
}
char* givebinary(int num, int n_bits){
	char* str = (char*)malloc(sizeof(char)*(n_bits + 1));
	int n = n_bits - 1;
	str[n_bits] = '\0';
	while(num != 0){
		str[n--] = (num % 2) + '0';
		num = num / 2;
	}
	while(n >= 0)
		str[n--] = '0';
	return str;
}

