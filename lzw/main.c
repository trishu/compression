#include<stdio.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>
#include"table.h"
void decode(int fdr, int fdw, table* t, int* arr);
void compress(int fdr, int fdw, table* t, int* arr);
void readheader(int fdr, int* arr);
void counttablesize(int fdr, int* arr);
int main(int agrc, char* agrv[]){
	if(agrc < 4 || agrc > 4){
		printf("Usage: ./project operation filename1 filename2");
		printf("operation :\n\t-c2 lzw compression\n\t-uc2 lzw decompression\n");
		exit(1);
	}
	if(strcmp(agrv[1], "-c2") == 0){
		int arr[256], i, fdw, fdr;
		for(i = 0; i < 256; i++){
			arr[i] = 0;
		}
		fdr = open(agrv[2], O_RDONLY);
		if(fdr == -1){
			printf("open fail\n");
			return 1;
		}
		counttablesize(fdr, arr);
		table t;
		inittable(&t, arr, 0);
		close(fdr);
		fdr = open(agrv[2], O_RDONLY);
		if(fdr == -1){
			printf("open fail\n");
			return 1;
		}
		fdw = open(agrv[3], O_WRONLY | O_CREAT, S_IRUSR);
		if(fdw == -1){
			printf("open fail\n");
			return 1;
		}
		compress(fdr, fdw, &t, arr);
	}
	else if(strcmp(agrv[1], "-uc2") == 0){
		table t;
		int arr[256];
		int fdr, fdw;
		fdr = open(agrv[2], O_RDONLY);
		if(fdr == -1){
			printf("open fail\n");
			return 1;
		}
		readheader(fdr, arr);
		inittable(&t, arr, 1);
		fdw = open(agrv[3], O_WRONLY|O_CREAT, S_IRUSR);
		if(fdw == -1){
			printf("open fail\n");
			return 1;
		}
		decode(fdr, fdw, &t, arr);
	}
	return 0;
}
