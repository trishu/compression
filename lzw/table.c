#include"table.h"
#include<stdio.h>
void inittable(table* t, int* arr, int type){
	int i = 0, j = 0, k = 0;
	for(i = 0; i < 256; i++){
		if(arr[i] == 1){
			t->th[k].first = j++;
			t->th[k].prefix = -1;
			t->th[k].B = (char)i;
			t->th[k].next = -1;
			k++;
		}
	}
	t->n = k;
	t->initial_size = k;
	for(i = k; i < MAX; i++){
		t->th[i].first = i;
		t->th[i].next = -1;
	}
	t->bitsize = FindBitRequire(k, type);
	t->max_index_value = FindMaxValue(t->bitsize, type);
	if(type == 0){
		if(t->n > t->max_index_value){
			t->bitsize++;
			t->max_index_value = FindMaxValue(t->bitsize, type);
		}
	}
	else if(type == 1){
		if(t->n == t->max_index_value){
			t->bitsize++;
			t->max_index_value = FindMaxValue(t->bitsize, type);
		}
	}
}
void addentry(table* t, int prefix, char B, int type){
	t->th[t->n].B = B;
	t->th[t->n].prefix = prefix;
	t->th[prefix].next = t->n;
	t->n++;
	if(type == 0){
	if(t->n > t->max_index_value){
		t->bitsize++;
		t->max_index_value = FindMaxValue(t->bitsize, type);
	}
	}
	else if(type == 1){
	if(t->n == t->max_index_value){
		t->bitsize++;
		t->max_index_value = FindMaxValue(t->bitsize, type);
	}
		
	}
}
int isfull(table* t){
	return t->n == MAX - 1;
}
int search(table* t, int prefix, char B){
	int i;
	for(i = 0; i < t->n; i++){
		if(t->th[i].B == B && t->th[i].prefix == prefix)
			return i;
	}
	return -1;
}
/*int search(table*t, int prefix, char B){
	int i, x, first;
	for(i = 0; i < t->initial_size; i++){
		if(t->th[i].B == B){
			if(t->th[i].prefix == prefix)
				return i;
			else{
				x = i;
				break;
			}
		}
	}
	if(i == t->initial_size)
		return -1;
	first = t->th[x].next;
	while(first < t->n){
		if(t->th[first].next != -1){
			if(t->th[first].prefix == prefix){
				return first;
			}
			else{
				first = t->th[first].next;
			}
		}
		else{
			if(t->th[first].prefix == prefix)
				return first;
			else
				break;
		}
	}
	return -1;
}*/
void printtable(table* t){
	int i;
	for(i = 0; i < t->n; i++){
		printf("%4d, %4d, %3c, %4d\n",t->th[i].first, t->th[i].prefix,
			t->th[i].B, t->th[i].next);
	}
	printf("%d, %d, %d, %d\n", t->initial_size, t->n, t->bitsize, t->max_index_value);
}
