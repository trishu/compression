#define MAX 4096
typedef struct data{
	int first;
	int prefix;
	char B;
	int next;
}data;
typedef struct table{
	data th[MAX];
	int initial_size;
	int n;
	int bitsize;
	int max_index_value;
}table;
void inittable(table* t, int* arr, int type);
void inittable2(table* t);
void addentry(table* t, int prefix, char B, int type);
int isfull(table* t);
int search(table*t, int prefix, char B);
void printtable(table* t);
int FindBitRequire(int num, int type);
int FindMaxValue(int n, int type);
char* givebinary(int num, int bit_size);
